package net.xsoft.duo.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import net.xsoft.duo.R;
import net.xsoft.duo.adapters.ViewPagerAdapter;
import net.xsoft.duo.database.Database;
import net.xsoft.duo.fragments.DuaFragment;
import net.xsoft.duo.fragments.FavouriteFragment;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Database.init(getApplicationContext());

        ViewPager viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DuaFragment(), "DUOLAR");
        adapter.addFragment(new FavouriteFragment(), "SEVIMLI");
        viewPager.setAdapter(adapter);
    }
}
