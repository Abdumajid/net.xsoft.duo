package net.xsoft.duo.activities;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toolbar;

import net.xsoft.duo.R;
import net.xsoft.duo.adapters.MyFragmentPagerAdapter;
import net.xsoft.duo.database.Database;
import net.xsoft.duo.models.DuaModel;
import net.xsoft.duo.utils.Utility;

import java.io.IOException;
import java.util.ArrayList;

public class VPActivity extends FragmentActivity implements SeekBar.OnSeekBarChangeListener, MediaPlayer.OnCompletionListener {
    private SeekBar seekBar;
    private ImageView playAudio;
    private ArrayList<DuaModel> list;
    private ArrayList<String> songsList;
    private ImageView star;
    private ViewPager pager;
    private Utility utils;
    private MediaPlayer mp;
    private Handler mHandler = new Handler();
    private int currentSongIndex = 0;
    private boolean isPause = true;
    private int position;
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            long currentDuration = 0;

            try {
                totalDuration = mp.getDuration();
                currentDuration = mp.getCurrentPosition();
                int progress = (int) (Utility.getProgressPercentage(currentDuration, totalDuration));
                seekBar.setProgress(progress);    /* Running this thread after 100 milliseconds */
                mHandler.postDelayed(this, 100);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private int id, type;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vp);
        initView();
//        setTitle("Duolar");
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        this.position = extras.getInt("position");
        id = extras.getInt("id");
        Log.d("POSITION", getPosition() + "");
        Log.d("ID", id + "");
        type = extras.getInt("type");
        Log.d("TYPE", type + "");

        utils = new Utility();
        list = new ArrayList<>();
        list = updateList(type);
        songsList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            songsList.add(i, list.get(i).getAudio());
        }
        mp = new MediaPlayer();
        mp.reset();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

        seekBar.setOnSeekBarChangeListener(this); // Important
        mp.setOnCompletionListener(this); // Important

        PagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), list);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(position);
        updateBottomToolbar(list.get(getPosition()));

        playAudio.setOnClickListener(v -> {
            if (mp.isPlaying()) {
                mp.pause();
                isPause = true;
                mHandler.removeCallbacks(mUpdateTimeTask);
                playAudio.setImageResource(R.drawable.ic_play);
            }
            if (isPause) {
                mp.start();
                isPause = false;
                updateProgressBar();
                playAudio.setImageResource(R.drawable.ic_pause);
            }
            if (!mp.isPlaying()) {
                playSong(getPosition());
            }
        });
        star.setOnClickListener(v -> {
            Log.d("TAG", Database.getInstance().getFavouriteID(list.get(getPosition()).getId()) ? "1" : "0");
            if (list.get(getPosition()).getFavourite() > 0) {
                Log.d("TAG", Database.getInstance().getFavouriteID(list.get(getPosition()).getId()) ? "1" : "0");
                star.setImageResource(R.drawable.ic_star_border);
                Database.getInstance().changeFavourite(list.get(getPosition()).getId(), 0);
                list.get(getPosition()).setFavourite(0);
            } else {
                star.setImageResource(R.drawable.ic_star);
                Database.getInstance().changeFavourite(list.get(getPosition()).getId(), 1);
                list.get(getPosition()).setFavourite(1);
            }
        });

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setPosition(position);
                updateBottomToolbar(list.get(position));
                if (position != currentSongIndex) {
                    stopTreck();
                    // Swipe to left
                    if (position < currentSongIndex) {
                        preousTreck();
                    }
                    // Swipe to rigth
                    else {
                        nextTreck();
                    }
                }

                Log.d("POSITION AFTER", getPosition() + "");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
    private void stopTreck() {
        mp.stop();
        seekBar.setProgress(0);
        mHandler.removeCallbacks(mUpdateTimeTask); /* Progress Update stop */
        playAudio.setImageResource(R.drawable.ic_play);
        isPause = true;
    }

    private void nextTreck() {
        if (currentSongIndex < (songsList.size() - 1)) {
            currentSongIndex = currentSongIndex + 1;
        } else {
            playSong(0);
            currentSongIndex = 0;
        }
    }

    private void preousTreck() {
        if (currentSongIndex > 0) {
            currentSongIndex = currentSongIndex - 1;
        } else {
            currentSongIndex = songsList.size() - 1;
        }
    }

    public void playSong(int songIndex) {
        // Play song
        try {
            mp.reset();
            Uri myUri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + songsList.get(songIndex));
            mp.setDataSource(this, myUri);
            mp.prepare();
            mp.setOnPreparedListener(mp -> {
                try {
                    mp.start();
                    updateProgressBar();
                    playAudio.setImageResource(R.drawable.ic_pause);
                } catch (Exception e) {
                    Log.i("EXCEPTION", "" + e.getMessage());
                }
            });
            playAudio.setImageResource(R.drawable.ic_pause);

            // set Progress bar values
            seekBar.setProgress(0);
            seekBar.setMax(100);

            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private void initView() {
        pager = findViewById(R.id.pager);
        seekBar = findViewById(R.id.id_seekbar);
        playAudio = findViewById(R.id.play_song);
        star = findViewById(R.id.id_sevimli);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mp.isPlaying())
            mp.stop();
        mp.release();
    }

    private void updateBottomToolbar(DuaModel model) {
        if (model.getFavourite() > 0)
            star.setImageResource(R.drawable.ic_star);
        else star.setImageResource(R.drawable.ic_star_border);
    }

    public ArrayList<DuaModel> updateList(int type) {
        if (type == 1) {
            return Database.getInstance().getDuaData();
        } else
            return Database.getInstance().getDuaFavorite();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);

    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
        mp.seekTo(currentPosition);
        updateProgressBar();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
      stopTreck();
    }
}
