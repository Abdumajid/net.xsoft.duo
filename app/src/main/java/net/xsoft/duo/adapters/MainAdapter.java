package net.xsoft.duo.adapters;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.xsoft.duo.R;
import net.xsoft.duo.models.DuaModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by User on 3/22/2018.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private int type;
    private ArrayList<DuaModel> list;
    private OnItemClickListener onItemClickListener;

    public MainAdapter(ArrayList<DuaModel> list, int type) {
        this.list = list;
        this.type = type;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DuaModel data = list.get(position);
        holder.bind(data, position);
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void removeAt(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

    public interface OnItemClickListener {
        void onItemClick(DuaModel data, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private ImageView star;
        private TextView textView;
        private DuaModel data;
        private int position;

        ViewHolder(View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.id_imageview);
            this.textView = itemView.findViewById(R.id.id_textView);
            this.star = itemView.findViewById(R.id.star);
            if (onItemClickListener != null)
                itemView.setOnClickListener(view -> onItemClickListener.onItemClick(data, position));
        }

        void bind(DuaModel data, int position) {
            this.data = data;
            this.position = position;
            if (data.getFavourite() > 0)
                star.setImageResource(R.drawable.ic_star);
            else
                star.setImageResource(R.drawable.ic_star_border);

            image.setImageDrawable(loadImage(data.getImage()));
            textView.setText(data.getTitle());
        }

        Drawable loadImage(String imageName) {
            try {
                InputStream ims = itemView.getContext().getAssets().open("image/" + imageName + ".png");
                return Drawable.createFromStream(ims, null);
            } catch (IOException ex) {
                return null;
            }
        }
    }
}


