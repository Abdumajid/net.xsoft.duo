package net.xsoft.duo.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.xsoft.duo.fragments.PageFragment;
import net.xsoft.duo.models.DuaModel;

import java.util.ArrayList;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<DuaModel> list;

    private DuaModel duaModel;
    private Context context;

    public MyFragmentPagerAdapter(FragmentManager fm, ArrayList<DuaModel> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(list.get(position).getId());
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public interface OnClickListener {
        void onClick(int itemposition);
    }
}
