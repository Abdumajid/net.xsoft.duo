package net.xsoft.duo.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.xsoft.duo.models.DuaModel;

import java.util.ArrayList;

public class Database extends DBHelper {
    @SuppressLint("StaticFieldLeak")
    private static Database database;

    private Database(Context context) {
        super(context, "dua.db");
    }

    public static void init(Context context) {
        if (database == null) {
            database = new Database(context);
        }
    }

    public static Database getInstance() {
        return database;
    }

    public ArrayList<DuaModel> getDuaData() {
        ArrayList<DuaModel> data = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM dua", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DuaModel duaModel = DuaModel.getInstance(cursor);
            data.add(duaModel);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }
    public ArrayList<DuaModel> getDuaFavorite() {
        ArrayList<DuaModel> data = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM dua where favourite > 0", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DuaModel duaModel = DuaModel.getInstance(cursor);
            data.add(duaModel);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }

    public void updateData(String table, ContentValues contentValues) {
        int id = (int) mDatabase.insertWithOnConflict(table, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            mDatabase.update(table, contentValues, "id=" + contentValues.getAsString("id"), null);
        }
    }
    public DuaModel getDataByID(int id) {
        DuaModel data;
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM dua WHERE uuid = " + id, null);
        cursor.moveToFirst();
        data = DuaModel.getInstance(cursor);
        cursor.close();
        return data;
    }
    public void changeFavourite(int id, int f){
//        mDatabase.rawQuery("Update dua set favourite = " + f +" where id = " + id, null);
        mDatabase.execSQL("Update dua set favourite = " + f +" where id = " + id);
    }
    public boolean getFavouriteID(int id){
        final String SELECT_FROM = "SELECT favourite " + "FROM dua "
                + "WHERE uuid = " + id;
        @SuppressLint("Recycle")
        Cursor cursor = mDatabase.rawQuery(SELECT_FROM,null);
        cursor.moveToFirst();
        int i = cursor.getInt(cursor.getColumnIndex("favourite"));
        cursor.close();
        return i > 0;
    }

}
