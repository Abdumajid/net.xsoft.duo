package net.xsoft.duo.database;

import net.xsoft.duo.models.DuaModel;

import java.util.ArrayList;

public class UpdateDatabase {
    private ArrayList<DuaModel> list;

    public void saveArrayList(ArrayList<DuaModel> list, String table){
        if (list!= null && list.size()>0){
            for (DuaModel duaModel : list) {
                Database.getInstance().updateData(table,duaModel.getContentValues());
            }
        }

    }
}
