package net.xsoft.duo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import net.xsoft.duo.R;
import net.xsoft.duo.activities.VPActivity;
import net.xsoft.duo.adapters.MainAdapter;
import net.xsoft.duo.database.Database;
import net.xsoft.duo.models.DuaModel;

import java.util.ArrayList;

public class DuaFragment extends Fragment implements MainAdapter.OnItemClickListener {
    private RecyclerView recyclerView;
    private MainAdapter adapter;
    private ArrayList<DuaModel> data;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dua, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = view.getContext();

        data = new ArrayList<>();
        this.recyclerView = view.findViewById(R.id.recyclerview);
        layoutManager = new GridLayoutManager(context, 2);
        getData();
    }

    private void getData() {
        data = Database.getInstance().getDuaData();
        adapter = new MainAdapter(data,1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAnimation(new AlphaAnimation(0.2f, 0.3f));
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void onItemClick(DuaModel data, int position) {
//        Toast.makeText(context, "" + data.getId(), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getActivity(), VPActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("id", data.getId());
        bundle.putInt("position", position);
        Log.d("POSITION_REC", position+"");
        bundle.putInt("type", 1);
        i.putExtras(bundle);
        startActivity(i);

    }
}
