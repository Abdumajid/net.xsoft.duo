package net.xsoft.duo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.xsoft.duo.R;
import net.xsoft.duo.activities.VPActivity;
import net.xsoft.duo.adapters.MainAdapter;
import net.xsoft.duo.database.Database;
import net.xsoft.duo.models.DuaModel;

import java.util.ArrayList;
import java.util.Objects;

public class FavouriteFragment extends Fragment implements MainAdapter.OnItemClickListener {
    private RecyclerView recyclerView;
    private MainAdapter adapter;
    private ArrayList<DuaModel> list;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;

    public FavouriteFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favourite, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = view.getContext();
        list = new ArrayList<>();


    }

    @Override
    public void onResume() {
        super.onResume();
        list = Database.getInstance().getDuaFavorite();
        adapter = new MainAdapter(list, 2);
        adapter.notifyDataSetChanged();
        recyclerView = Objects.requireNonNull(getView()).findViewById(R.id.recyclerview);
        layoutManager = new GridLayoutManager(context, 2);
        recyclerView.setLayoutManager(layoutManager);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(DuaModel data, int position) {
        if (adapter.getItemId(position) == R.id.id_delete) {
            adapter.removeAt(position);
        }
        Intent intent = new Intent(getActivity(), VPActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("id", data.getId());
        bundle.putInt("position", position);
        bundle.putInt("type", 2);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
