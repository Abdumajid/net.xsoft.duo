package net.xsoft.duo.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import net.xsoft.duo.R;
import net.xsoft.duo.database.Database;
import net.xsoft.duo.interfaces.OnPlayClickListener;
import net.xsoft.duo.models.DuaModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class PageFragment extends Fragment {
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    int id;
    private DuaModel data;

    public static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    public DuaModel getData() {
        return data;
    }

    public void setData(DuaModel data) {
        this.data = data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        id = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        this.data = Database.getInstance().getDataByID(id);
        getActivity().setTitle("Title");
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.fragment, null);
        TextView title = view.findViewById(R.id.id_title);
        title.setText(data.getTitle());
        ImageView image = view.findViewById(R.id.id_image);
        image.setImageDrawable(loadImage(data.getImage()));
        TextView description = view.findViewById(R.id.id_desc);
        description.setText(data.getTitle());
        TextView textArabian = view.findViewById(R.id.id_dua_arabian);
        textArabian.setText(data.getDuaarabic());
        TextView duaLatin = view.findViewById(R.id.id_desc_latin);
        duaLatin.setText(data.getDualatin());
        TextView afterDua = view.findViewById(R.id.id_desc_qisqacha);
        afterDua.setText(data.getAfterdua());

        TextView mano = view.findViewById(R.id.id_desc_mano);
        mano.setText(data.getTarjima());
        return view;
    }

    Drawable loadImage(String imageName) {
        try {
            InputStream ims = Objects.requireNonNull(getActivity()).getAssets().open("image/" + imageName + ".png");
            return Drawable.createFromStream(ims, null);
        } catch (IOException ex) {
            return null;
        }
    }

}
