package net.xsoft.duo.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by User on 3/22/2018.
 */
@Entity(tableName = "dua")
public class DuaData {
    @PrimaryKey
    private int id;
    private String title;
    private String image;
    private String duaarabic;
    private String dualatin;
    private String tarjima;
    private String audio;
    private int favourite;
    private String beforedua;
    private String afterdua;
    private String dalil;

    public String getBeforedua() {
        return beforedua;
    }

    public void setBeforedua(String beforedua) {
        this.beforedua = beforedua;
    }

    public String getAfterdua() {
        return afterdua;
    }

    public void setAfterdua(String afterdua) {
        this.afterdua = afterdua;
    }

    public String getDalil() {
        return dalil;
    }

    public void setDalil(String dalil) {
        this.dalil = dalil;
    }

    public int getFavourite() {
        return favourite;
    }

    public void setFavourite(int favourite) {
        this.favourite = favourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuaarabic() {
        return duaarabic;
    }

    public void setDuaarabic(String duaarabic) {
        this.duaarabic = duaarabic;
    }

    public String getDualatin() {
        return dualatin;
    }

    public void setDualatin(String dualatin) {
        this.dualatin = dualatin;
    }

    public String getTarjima() {
        return tarjima;
    }

    public void setTarjima(String tarjima) {
        this.tarjima = tarjima;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

}
