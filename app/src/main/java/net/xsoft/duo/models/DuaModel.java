package net.xsoft.duo.models;

import android.content.ContentValues;
import android.database.Cursor;

public class DuaModel {
    private int id;
    private String title;
    private String image;
    private String duaarabic;
    private String dualatin;
    private String tarjima;
    private String audio;
    private int favourite;
    private String beforedua;
    private String afterdua;
    private String dalil;

    public DuaModel(int id, String title,
                    String image, String duaarabic,
                    String dualatin, String tarjima,
                    String audio, int favourite,
                    String beforedua, String afterdua, String dalil) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.duaarabic = duaarabic;
        this.dualatin = dualatin;
        this.tarjima = tarjima;
        this.audio = audio;
        this.favourite = favourite;
        this.beforedua = beforedua;
        this.afterdua = afterdua;
        this.dalil = dalil;
    }

    public static DuaModel getInstance(Cursor c) {
        return new DuaModel(
                c.getInt(c.getColumnIndex("id")),
                c.getString(c.getColumnIndex("title")),
                c.getString(c.getColumnIndex("image")),
                c.getString(c.getColumnIndex("duaarabic")),
                c.getString(c.getColumnIndex("dualatin")),
                c.getString(c.getColumnIndex("tarjima")),
                c.getString(c.getColumnIndex("audio")),
                c.getInt(c.getColumnIndex("favourite")),
                c.getString(c.getColumnIndex("beforedua")),
                c.getString(c.getColumnIndex("afterdua")),
                c.getString(c.getColumnIndex("dalil"))
        );
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuaarabic() {
        return duaarabic;
    }

    public void setDuaarabic(String duaarabic) {
        this.duaarabic = duaarabic;
    }

    public String getDualatin() {
        return dualatin;
    }

    public void setDualatin(String dualatin) {
        this.dualatin = dualatin;
    }

    public String getTarjima() {
        return tarjima;
    }

    public void setTarjima(String tarjima) {
        this.tarjima = tarjima;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public int getFavourite() {
        return favourite;
    }

    public void setFavourite(int favourite) {
        this.favourite = favourite;
    }

    public String getBeforedua() {
        return beforedua;
    }

    public void setBeforedua(String beforedua) {
        this.beforedua = beforedua;
    }

    public String getAfterdua() {
        return afterdua;
    }

    public void setAfterdua(String afterdua) {
        this.afterdua = afterdua;
    }

    public String getDalil() {
        return dalil;
    }

    public void setDalil(String dalil) {
        this.dalil = dalil;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put("id", getId());
        cv.put("title", getTitle());
        cv.put("image", getImage());
        cv.put("duaarabic", getDuaarabic());
        cv.put("dualatin", getDualatin());
        cv.put("tarjima", getTarjima());
        cv.put("audio", getAudio());
        cv.put("favourite", getFavourite());
        cv.put("beforedua", getBeforedua());
        cv.put("afterdua", getAfterdua());
        cv.put("dalil", getDalil());
        return cv;
    }
}
