package net.xsoft.duo.models;

public class SampleModel {
    private int id;

    public SampleModel(int id, int position) {
        this.id = id;
        this.position = position;
    }

    private int position;
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public SampleModel(int id, int position, int type) {
        this.id = id;
        this.position = position;
        this.type = type;
    }
}
